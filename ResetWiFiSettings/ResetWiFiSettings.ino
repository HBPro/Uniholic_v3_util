#include "Arduino.h"
#include <ESP8266WiFi.h>

void setup() {
	Serial.begin(115200);
	Serial.println();

	WiFi.printDiag(Serial);

	if (WiFi.SSID().length() > 0) {

		WiFi.mode(WIFI_AP);
		WiFi.disconnect();
		ESP.eraseConfig();

		WiFi.printDiag(Serial);

		delay(3000);
		ESP.restart();
	}
}

void loop() {
}
