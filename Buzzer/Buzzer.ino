#include "Arduino.h"
#include <Wire.h>
#include <pcf8574.h>
#include "Note.h"

typedef enum {
	PASSIVE, ACTIVE
} buzzer_t;

buzzer_t type;
PCF8574 Pcf(0);

const int c = 261;
const int d = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int a = 440;
const int aS = 455;
const int b = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;

void Beep(uint8_t NumBeep, uint32_t Period, uint16_t Frequency = 0) {
	uint32_t halfcycle = 500000 / Frequency;
	switch (type) {
	case buzzer_t::PASSIVE:
		for (unsigned char i = 0; i < NumBeep; i++) {
			uint32_t t = millis() + Period;
			while (t > millis()) {
				Pcf.write(0, true);
				delayMicroseconds(halfcycle);
				Pcf.write(0, false);
				delayMicroseconds(halfcycle);
			}
			delay(Period);
		}
		break;
	case buzzer_t::ACTIVE:
		for (uint8_t i = 0; i < NumBeep; i++) {
			Pcf.write(0, true);
			delay(Period);
			Pcf.write(0, false);
			delay(Period);
		}
		break;
	default:
		break;
	}
}
void playNote(uint16_t tone, uint32_t duration) {
	uint32_t halfcycle = 500000 / tone;
	uint32_t t = millis() + duration;
	while (t > millis()) {
		Pcf.write(0, true);
		delayMicroseconds(halfcycle);
		Pcf.write(0, false);
		delayMicroseconds(halfcycle);
		yield();
	}

}

//void ImperialMarch() {
//
//playNote(LA3,Q);
//delay(1+Q); //delay duration should always be 1 ms more than the note in order to separate them.
//playNote(LA3,Q);
//delay(1+Q);
//playNote(LA3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//
//playNote(LA3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//playNote(LA3,H);
//delay(1+H);
//
//playNote(E4,Q);
//delay(1+Q);
//playNote(E4,Q);
//delay(1+Q);
//playNote(E4,Q);
//delay(1+Q);
//playNote(F4,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//
//playNote(Ab3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//playNote(LA3,H);
//delay(1+H);
//
//playNote(LA4,Q);
//delay(1+Q);
//playNote(LA3,E+S);
//delay(1+E+S);
//playNote(LA3,S);
//delay(1+S);
//playNote(LA4,Q);
//delay(1+Q);
//playNote(Ab4,E+S);
//delay(1+E+S);
//playNote(G4,S);
//delay(1+S);
//
//playNote(Gb4,S);
//delay(1+S);
//playNote(E4,S);
//delay(1+S);
//playNote(F4,E);
//delay(1+E);
//delay(1+E);//PAUSE
//playNote(Bb3,E);
//delay(1+E);
//playNote(Eb4,Q);
//delay(1+Q);
//playNote(D4,E+S);
//delay(1+E+S);
//playNote(Db4,S);
//delay(1+S);
//
//playNote(C4,S);
//delay(1+S);
//playNote(B3,S);
//delay(1+S);
//playNote(C4,E);
//delay(1+E);
//delay(1+E);//PAUSE QUASI FINE RIGA
//playNote(F3,E);
//delay(1+E);
//playNote(Ab3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(LA3,S);
//delay(1+S);
//
//playNote(C4,Q);
//delay(1+Q);
//playNote(LA3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//playNote(E4,H);
//delay(1+H);
//
//playNote(LA4,Q);
//delay(1+Q);
//playNote(LA3,E+S);
//delay(1+E+S);
//playNote(LA3,S);
//delay(1+S);
//playNote(LA4,Q);
//delay(1+Q);
//playNote(Ab4,E+S);
//delay(1+E+S);
//playNote(G4,S);
//delay(1+S);
//
//playNote(Gb4,S);
//delay(1+S);
//playNote(E4,S);
//delay(1+S);
//playNote(F4,E);
//delay(1+E);
//delay(1+E);//PAUSE
//playNote(Bb3,E);
//delay(1+E);
//playNote(Eb4,Q);
//delay(1+Q);
//playNote(D4,E+S);
//delay(1+E+S);
//playNote(Db4,S);
//delay(1+S);
//
//playNote(C4,S);
//delay(1+S);
//playNote(B3,S);
//delay(1+S);
//playNote(C4,E);
//delay(1+E);
//delay(1+E);//PAUSE QUASI FINE RIGA
//playNote(F3,E);
//delay(1+E);
//playNote(Ab3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//
//playNote(LA3,Q);
//delay(1+Q);
//playNote(F3,E+S);
//delay(1+E+S);
//playNote(C4,S);
//delay(1+S);
//playNote(LA3,H);
//delay(1+H);
//
//delay(2*H);
//}

void firstSection() {

playNote(a, 500);
playNote(a, 500);
playNote(a, 500);
playNote(f, 350);
playNote(cH, 150);
playNote(a, 500);
playNote(f, 350);
playNote(cH, 150);
playNote(a, 650);

delay(500);

playNote(eH, 500);
playNote(eH, 500);
playNote(eH, 500);
playNote(fH, 350);
playNote(cH, 150);
playNote(gS, 500);
playNote(f, 350);
playNote(cH, 150);
playNote(a, 650);

delay(500);
}

void secondSection() {
playNote(aH, 500);
playNote(a, 300);
playNote(a, 150);
playNote(aH, 500);
playNote(gSH, 325);
playNote(gH, 175);
playNote(fSH, 125);
playNote(fH, 125);
playNote(fSH, 250);

delay(325);

playNote(aS, 250);
playNote(dSH, 500);
playNote(dH, 325);
playNote(cSH, 175);
playNote(cH, 125);
playNote(b, 125);
playNote(cH, 250);

delay(350);
}

//The setup function is called once at startup of the sketch
void setup() {

	type = buzzer_t::ACTIVE;

	Wire.begin();

	byte error, address;
	for (address = 1; address < 127; address++) {

		Wire.beginTransmission(address);
		error = Wire.endTransmission();
		if (error == 0) {

			Pcf.setAddress(address);
		} else if (error == 4) {
		}
	}
}

// The loop function is called in an endless loop
void loop() {

//Beep(3, 1000, 4000);
//Play first section
	firstSection();

//Play second section
	secondSection();

//Variant 1
	playNote(f, 250);
	playNote(gS, 500);
	playNote(f, 350);
	playNote(a, 125);
	playNote(cH, 500);
	playNote(a, 375);
	playNote(cH, 125);
	playNote(eH, 650);

	delay(500);

//Repeat second section
	secondSection();

//Variant 2
	playNote(f, 250);
	playNote(gS, 500);
	playNote(f, 375);
	playNote(cH, 125);
	playNote(a, 500);
	playNote(f, 375);
	playNote(cH, 125);
	playNote(a, 650);

	delay(650);
}
