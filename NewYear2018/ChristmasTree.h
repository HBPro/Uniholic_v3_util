#ifndef CHRISTMASTREE_H_
#define CHRISTMASTREE_H_

#include "Note.h"

void ct() {
	playNote(_C4, 500);
	playNote(gS, 500);
	playNote(gS, 500);
	playNote(_Gb4, 500);
	playNote(gS, 500);
	playNote(_E4, 500);
	playNote(_C4, 500);
	playNote(_C4, 500);
	playNote(_C4, 500);
	playNote(gS, 500);
	playNote(gS, 500);
	playNote(_Gb4, 500);
	playNote(gS, 500);
	playNote(_B4, 500);
	delay(500);
	playNote(_B4, 500);
	playNote(_Db4, 500);
	playNote(_Db4, 500);
	playNote(_A4, 500);
	playNote(_A4, 500);
	playNote(gS, 500);
	playNote(_Gb4, 500);
	playNote(_E4, 500);
	playNote(_C4, 500);
	playNote(gS, 500);
	playNote(gS, 500);
	playNote(_Gb4, 500);
	playNote(gS, 500);
	playNote(_E4, 500);
}

void test() {
	playNote(_C4, 500);
	playNote(_A4, 500);
	playNote(_A4, 500);
	playNote(_G4, 500);
	playNote(_A4, 500);
	playNote(_F4, 500);
	playNote(_C4, 500);
	playNote(_C4, 500);
	playNote(_C4, 500);
	playNote(_A4, 500);
	playNote(_A4, 500);
	playNote(_Bb4, 500);
	playNote(_G4, 500);
	playNote(_C5, 1500);
	playNote(_C5, 500);
	playNote(_D4, 500);
	playNote(_D4, 500);
	playNote(_Bb4, 500);
	playNote(_Bb4, 500);
	playNote(_A4, 500);
	playNote(_G4, 500);
	playNote(_F4, 500);
	playNote(_C4, 500);
	playNote(_A4, 500);
	playNote(_A4, 500);
	playNote(_G4, 500);
	playNote(_A4, 500);
	playNote(_F4, 1500);
}

#endif /* CHRISTMASTREE_H_ */
