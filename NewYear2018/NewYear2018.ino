#include "Arduino.h"
#include <string>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <SPI.h>
#include <Adafruit_ILI9341.h>
#include <Adafruit_mfGFX.h>
#include "Extgpio.h"
#include "Images.h"
#include "Note.h"
#include "ChristmasTree.h"

Adafruit_ILI9341 tft = Adafruit_ILI9341(15, 2);
Extgpio egpio;

void playNote(uint16_t tone, uint32_t duration) {
	Serial.println(tone);
	uint32_t halfcycle = 500000 / tone;
	uint32_t t = millis() + duration;
	while (t > millis()) {
		egpio.toggle(0);
		delayMicroseconds(halfcycle);
	}
	yield();
}

void setup() {

	Serial.begin(115200);
	Serial.println();

	WiFi.mode(WIFI_OFF);

	Wire.begin();
	Wire.setClock(700000L);
	tft.begin();
	tft.fillScreen(0xFFFF);
	tft.setRotation(3);

	tft.setAddrWindow((tft.width() - HBPro_NewYear.width) / 2,
			(tft.height() - HBPro_NewYear.height) / 2,
			(tft.width() - HBPro_NewYear.width) / 2 + HBPro_NewYear.width - 1,
			(tft.height() - HBPro_NewYear.height) / 2 + HBPro_NewYear.height
					- 1);
	auto i_max = HBPro_NewYear.width * HBPro_NewYear.height;
	for (auto i = 0; i < i_max; i++) {
		tft.pushColor(pgm_read_word(&(image_data_HBPro_NewYear[i])));
	}

	byte error, address;
	for (address = 1; address < 127; address++) {

		Wire.beginTransmission(address);
		error = Wire.endTransmission();

		if (error == 0) {
			Serial.println("Wire address=" + (String) address);
			egpio.setAddress(address);
			egpio.detectType();
			egpio.begin();
			egpio.pinMode(0, OUTPUT);
			break;
		}
	}
	/*
	 WiFi.mode(WIFI_STA);

	 WiFi.setAutoConnect(true);
	 WiFi.setAutoReconnect(true);
	 WiFi.reconnect();
	 WiFi.waitForConnectResult();

	 while (WiFi.status() != WL_CONNECTED) {
	 delay(500);
	 Serial.println("WiFi not connected");
	 WiFi.beginSmartConfig();

	 if (WiFi.smartConfigDone()) {
	 Serial.println("SmartConfig Success");
	 break;
	 }
	 }

	 Serial.println();

	 WiFi.printDiag(Serial);

	 Serial.println();
	 Serial.println(WiFi.localIP());
	 */
	//ct();
test();

	egpio.digitalWrite(0, LOW);

	tft.fillScreen(0xFFFF);
	tft.setAddrWindow((tft.width() - beer.width) / 2,
			(tft.height() - beer.height),
			(tft.width() - beer.width) / 2 + beer.width - 1,
			(tft.height() - beer.height) + beer.height - 1);
	i_max = beer.width * beer.height;
	for (auto i = 0; i < i_max; i++) {
		tft.pushColor(pgm_read_word(&(image_data_beer[i])));
	}

	tft.setTextColor(ILI9341_RED);
	tft.setFont(OPENSANS_18);
	tft.printAt("� ����������� !!!", 160, 20, Center);
	tft.printAt("���� ������� ����� !!!", 160, 50, Center);

}

void loop() {

}
