#include "Extgpio.h"
#include <Arduino.h>

extern String CharToByteStr(uint8_t v);


Extgpio::Extgpio() {
}

Extgpio::~Extgpio() {
}

void Extgpio::begin() {
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
		writeRegister(MCP23008_IODIR, B11111111); //all input

		//writeRegister(MCP23008_IODIR, B00000000); //all output
		//writeRegister(MCP23008_GPIO, B11111111); //all off
		//writeRegister(MCP23008_GPIO, B00000000); //all on
		break;
	default:
		break;
	}
}

uint8_t Extgpio::getAddress() const {
	return _address;
}

void Extgpio::setAddress(uint8_t address) {
	_address = address;
}

/**
 * Reads a given register
 */
uint8_t Extgpio::readRegister(uint8_t reg) {
	uint8_t data;
	Wire.beginTransmission(_address);
	Wire.write((uint8_t) reg);
	Wire.endTransmission();
	Wire.requestFrom((uint32_t) _address, 1);
	data = Wire.read();
	_error = Wire.endTransmission();
	return data;
}

/**
 * Writes a given register
 */
void Extgpio::writeRegister(uint8_t reg, uint8_t val) {
	Wire.beginTransmission(_address);
	Wire.write(reg);
	Wire.write(val);
	_error = Wire.endTransmission();
}

void Extgpio::digitalReadAll() {
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
		_data = readRegister(MCP23008_GPIO);
		break;
	case EXTGPIO_PCF8574:
		//_data = read8();
		break;
	}
}

void Extgpio::digitalWriteAll(uint8_t value) {
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
		writeRegister(MCP23008_GPIO, value);
		break;
	case EXTGPIO_PCF8574:
		write8(value);
		break;
	}
}

uint8_t Extgpio::digitalRead(uint8_t pin) {
	uint8_t value = 0;
	digitalReadAll();
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
	case EXTGPIO_PCF8574:
		value = (_data >> pin) & 0x1;
		break;
	}
	return value;
}

void Extgpio::digitalWrite(uint8_t pin, uint8_t value) {
	uint8_t gpio;
	digitalReadAll();
	gpio = _data;
	if (value == HIGH) {
		gpio |= 1 << pin;
	} else {
		gpio &= ~(1 << pin);
	}
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
	case EXTGPIO_PCF8574:
		digitalWriteAll(value);
		break;
	}
}

uint8_t Extgpio::value() {
	return _data;
}

void Extgpio::pinMode(uint8_t pin, uint8_t mode) {
	switch ((uint8_t) _type) {
	case EXTGPIO_MCP23008:
		uint8_t iodir;
		iodir = readRegister(MCP23008_IODIR);
		if (mode == INPUT) {
			iodir |= 1 << pin;
		} else {
			iodir &= ~(1 << pin);
		}
		writeRegister(MCP23008_IODIR, iodir);
		break;
	}
}

void Extgpio::toggle(uint8_t pin) {
	uint8_t gpio;
	digitalReadAll();
	gpio = _data;
	gpio ^= 1 << pin;
	digitalWriteAll(gpio);
}

uint8_t Extgpio::read8() {
	uint8_t data = 0;
	switch ((uint8_t) _type) {
	case EXTGPIO_PCF8574:
		Wire.beginTransmission(_address);
		Wire.requestFrom(_address, (uint8_t) 1);
		data = Wire.read();
		_error = Wire.endTransmission();
		break;
	}
	return data;
}

void Extgpio::write8(uint8_t value) {
	switch ((uint8_t) _type) {
	case EXTGPIO_PCF8574:
		Wire.beginTransmission(_address);
		_data = value;
		Wire.write(_data);
		_error = Wire.endTransmission();
		break;
	}
}

void Extgpio::detectType() {
	_type = EXTGPIO_MCP23008;
	writeRegister(MCP23008_IODIR, B11111111);
	if (!readRegister(MCP23008_IODIR)) {
		_type = EXTGPIO_PCF8574;
	}
}
