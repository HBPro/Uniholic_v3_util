#ifndef DVC_EXTGPIO_H_
#define DVC_EXTGPIO_H_

#include <Arduino.h>
#include <Wire.h>

// registers
#define MCP23008_IODIR		0x00
#define MCP23008_IPOL		0x01
#define MCP23008_GPINTEN	0x02
#define MCP23008_DEFVAL		0x03
#define MCP23008_INTCON		0x04
#define MCP23008_IOCON		0x05
#define MCP23008_GPPU		0x06
#define MCP23008_INTF		0x07
#define MCP23008_INTCAP		0x08
#define MCP23008_GPIO		0x09
#define MCP23008_OLAT		0x0A

//#define MCP23017_IODIRA		0x00
//#define MCP23017_IODIRB		0x01
//#define MCP23017_IPOLA		0x02
//#define MCP23017_IPOLB		0x03
//#define MCP23017_GPINTENA	0x04
//#define MCP23017_GPINTENB	0x05
//#define MCP23017_DEFVALA	0x06
//#define MCP23017_DEFVALB	0x07
//#define MCP23017_INTCONA	0x08
//#define MCP23017_INTCONB	0x09
//#define MCP23017_IOCONA		0x0A
//#define MCP23017_IOCONB		0x0B
//#define MCP23017_GPPUA		0x0C
//#define MCP23017_GPPUB		0x0D
//#define MCP23017_INTFA		0x0E
//#define MCP23017_INTFB		0x0F
//#define MCP23017_INTCAPA	0x10
//#define MCP23017_INTCAPB	0x11
//#define MCP23017_GPIOA		0x12
//#define MCP23017_GPIOB		0x13
//#define MCP23017_OLATA		0x14
//#define MCP23017_OLATB		0x15
//#define MCP23017_INT_ERR	255

#define PIN_ERROR			0x81

class Extgpio {
public:

	typedef enum {
		EXTGPIO_UNKNOWN, EXTGPIO_MCP23008, EXTGPIO_PCF8574
	//,EXTGPIO_PCF8575,EXTGPIO_MCP23017
	} extgpio_type_t;

	extgpio_type_t _type = EXTGPIO_UNKNOWN;

	Extgpio();
	~Extgpio();

	void begin();

	uint8_t getAddress() const;
	void setAddress(uint8_t address);

	void pinMode(uint8_t pin, uint8_t mode);
	void pullUp(uint8_t pin, uint8_t mode);

	uint8_t value();
	uint8_t lastError();

	uint8_t readRegister(uint8_t addr);
	void writeRegister(uint8_t addr, uint8_t value);

	uint8_t read8();
	void write8(uint8_t value);

	void digitalReadAll();
	uint8_t digitalRead(uint8_t pin);

	void digitalWriteAll(uint8_t value);
	void digitalWrite(uint8_t pin, uint8_t value);

	void toggle(uint8_t pin);
	void toggleAll();

	void detectType();

private:

	uint8_t _address = 0x20;
	uint16_t _data = 0;
	uint8_t _error = 0;

};

#endif /* DVC_EXTGPIO_H_ */
