#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <EEPROM.h>
#include <Ticker.h>
#include "SketchVersion.h"

#define LED_PIN     16        // Led in NodeMCU at pin GPIO16 (D0).

const String URL_FIRMWARE_PATH = "http://01.hbpro.ru/firmware/uniholic/";
const uint8_t COUNT_ATTEMPTS_INSTALL_FIRMWARE = 10;
const uint16_t EEPROM_SIZE = 1024;

uint8_t brightness = 0;
uint8_t brightness_max = 240;
Ticker ticker;
bool s;

/**
 * Install work firmware
 * @return result
 */
boolean updateFirmware(void) {
	boolean result = false;

	t_httpUpdate_return ret = ESPhttpUpdate.update(URL_FIRMWARE_PATH + "Uniholic_v3.bin", SKETCH_VERSION);
	switch (ret) {
	case HTTP_UPDATE_FAILED:
		Serial.printf("HTTP_UPDATE_FIRMWARE_FAILED Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
		Serial.println();
		break;

	case HTTP_UPDATE_NO_UPDATES:
		Serial.println("HTTP_UPDATE_FIRMWARE_NO_UPDATES");
		break;

	case HTTP_UPDATE_OK:
		Serial.println("HTTP_UPDATE_FIRMWARE_OK");
		result = true;
		break;
	}

	return result;
}

/**
 * Clear EEPROM (write all sector value 0)
 */
void EepromClean() {
	EEPROM.begin(EEPROM_SIZE);
	for (uint16_t i = 0; i < EEPROM_SIZE; i++) {
		if (EEPROM.read(i) != 255) {
			EEPROM.write(i, 255);
		}
	}
	EEPROM.commit();
	EEPROM.end();
}

void pulse() {
	if(s) {
		if(brightness != brightness_max) {
			brightness +=8;
		} else {
			s = false;
			brightness -=8;
		}
	} else {
		if(brightness == 0) {
			s = true;
			brightness +=8;
		} else {
			brightness -=8;
		}
	}
	//Serial.println(brightness);
	analogWrite(LED_PIN, brightness);
}

void setup() {
	analogWriteRange(255);
	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, LOW);

	Serial.begin(115200);
	Serial.println();
	Serial.println(F("Start smartconfig"));
	Serial.print(F("Compiled: "));
	Serial.print((String) __DATE__);
	Serial.print(F(" "));
	Serial.println((String) __TIME__);
	Serial.print(F("Firmware version: "));
	Serial.println((String) SKETCH_VERSION);

	EepromClean();

	ESP.eraseConfig();

	WiFi.disconnect(true);
	WiFi.setAutoConnect(true);
	WiFi.setAutoReconnect(true);
	WiFi.mode(WIFI_OFF);
	delay(500);

	WiFi.beginSmartConfig();

	s = true;
	ticker.attach_ms(24, pulse);

	while (WiFi.status() != WL_CONNECTED) {
		delay(250);
		if (!(WiFi.smartConfigDone())) {
			Serial.println(F("WiFi not connected"));
		}
	}
	ticker.detach();

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
	Serial.print("MAC: ");
	Serial.println(WiFi.macAddress());

	analogWrite(LED_PIN, 0);

	for (uint8_t j = 1; j <= COUNT_ATTEMPTS_INSTALL_FIRMWARE; j++) {
		Serial.println("Install firmware Uniholic attempt " + (String) j);
		if (updateFirmware()) {
			break;
		}
		delay(2000);
	}
	Serial.println(F("Not install work firmware"));
	Serial.println(F("Reboot the device and try again"));
}


void loop() {}
