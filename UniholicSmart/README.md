SmartConfig
---

Скачиваем архив UniholicSmart.zip

Извлекаем из него прошивку UniholicSmart.bin

Скачиваем на телефон приложение [ESP8266 SmartConfig](https://play.google.com/store/apps/details?id=com.cmmakerclub.iot.esptouch&hl=ru)

Прошиваем с помощью [NodeMCU Flasher](https://github.com/nodemcu/nodemcu-flasher/blob/master/Win32/Release/ESP8266Flasher.exe) по адресу 0x0 (модуль esp8266 необходимо извлечь из платы расширения)

После прошивки запускаем программу и настраиваем подключение к домашней сети.

После ждем примерно минуту, пока загрузится прошивка.

После загрузки прошивки модуль перезагрузится и будет видна сетевая активность по синему светодиоду на плате можуля.

Отключаем от питания, вставляем в плату расширения и дальше настраиваем под себя.

[YouTube Uniholic SmartConfig](https://www.youtube.com/watch?v=CzN48mYv-lo)