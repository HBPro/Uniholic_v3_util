#include "Arduino.h"
#include <EEPROM.h>

#define EEPROM_SIZE 1024

void EepromValueUpdate(uint32_t address, uint8_t value) {
	if (EEPROM.read(address) != value) {
		EEPROM.write(address, value);
	}
}

void EepromDump() {
	for (auto i = 0; i < EEPROM_SIZE; i++) {
		Serial.print(F("Address="));
		Serial.print(i);
		Serial.print(F(" value="));
		Serial.println(EEPROM.read(i));
		yield();
	}
}

void EepromClear() {
	for (auto i = 0; i < EEPROM_SIZE; i++) {
		EepromValueUpdate(i, 255);
		yield();
	}
	EEPROM.commit();
}

void setup() {
	Serial.begin(115200);
	Serial.println();

	EEPROM.begin(EEPROM_SIZE);

	Serial.println(F("Dump EEPROM"));
	Serial.println(F("-------------------------------------------"));
	EepromDump();
	Serial.println();

	Serial.println(F("Clear EEPROM"));
	EepromClear();
	Serial.println();

	Serial.println(F("Dump EEPROM"));
	Serial.println(F("-------------------------------------------"));
	EepromDump();
	Serial.println();
}

void loop() {
}
