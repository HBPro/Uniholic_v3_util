#include "Arduino.h"
#include "ESP8266WiFi.h"

String WiFiUrlManage() {
	String mac;
	mac = WiFi.macAddress();
	mac.replace(":", "");

	String url;
	url += F("http://01.hbpro.ru/device/");
	url += mac;
	return url;
}

void setup() {
// Add your initialization code here
	Serial.begin(115200);
	WiFi.begin();

	Serial.println();

	Serial.println(F("WiFi.printDiag():"));
	WiFi.printDiag(Serial);
	Serial.println();

	Serial.println("Manager URL:");
	Serial.println(WiFiUrlManage());

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}

void loop() {
}
