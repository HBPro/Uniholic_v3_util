#include <SPI.h>
#include <Adafruit_ILI9341.h>
#include <Adafruit_mfGFX.h>

Adafruit_ILI9341 tft = Adafruit_ILI9341(15, 2);

void setup() {

	char str[20];

	uint32_t realSize = ESP.getFlashChipRealSize();
	uint32_t ideSize = ESP.getFlashChipSize();
	FlashMode_t ideMode = ESP.getFlashChipMode();

	// put your setup code here, to run once:
	tft.begin();
	tft.fillScreen(ILI9341_BLACK);
	tft.setRotation(3);

	tft.setTextColor(ILI9341_WHITE);
	tft.setFont(OPENSANS_12);
	tft.printAt("��������������:", 10, 10, Left);

	sprintf(str, "Flash real id:   %08X\n", ESP.getFlashChipId());
	tft.printAt(str, 10, 30, Left);

	sprintf(str, "Flash real size: %u\n\n", realSize);
	tft.printAt(str, 10, 50, Left);

	sprintf(str, "Flash ide  size: %u\n", ideSize);
	tft.printAt(str, 10, 70, Left);

	sprintf(str, "Flash ide speed: %u\n", ESP.getFlashChipSpeed());
	tft.printAt(str, 10, 90, Left);

	sprintf(str, "Flash ide mode:  %s\n",
			(ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" :
				ideMode == FM_DIO ? "DIO" :
				ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
	tft.printAt(str, 10, 110, Left);
}

void loop() {
}
